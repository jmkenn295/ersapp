package com.services;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import com.dao.IReimbDao;
import com.dao.ReimbDao;
import com.models.Reimbursement;

public class ReimbursementService {
    private static ReimbDao reimbdao = new IReimbDao();

    public static List<Reimbursement> getReimbsByAuthor(int author) throws IOException, SQLException {
        return reimbdao.getReimbsByAuthor(author);
    }

    public static List<Reimbursement> getAllReimbs() throws IOException, SQLException {
        return reimbdao.getAllReimbs();
    }

    public static Reimbursement addReimbursement(Reimbursement reimb) throws IOException, SQLException {
        return reimbdao.addReimb(reimb);
    }

    public static boolean updateReimbursement(Reimbursement reimb) throws IOException, SQLException {
        return reimbdao.updateReimb(reimb);
    }
}
