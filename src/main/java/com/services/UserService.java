package com.services;


import java.io.IOException;
import java.sql.SQLException;
import com.dao.UserDao;
import com.dao.IUserDao;
import com.models.User;



public class UserService {
    UserDao userDao = new IUserDao();

    public User getUserByUserId(int userId) throws IOException, SQLException {
        return userDao.getUserByUserId(userId);
    }

    public User addUser(User newUser) throws IOException, SQLException {
        if (!usernameExists(newUser.getUsername()))
            newUser = userDao.addUser(newUser);

        return newUser;
    }

    public User getUserByCredentials(String username, String password) throws SQLException, IOException {
        return userDao.getUserByCredentials(username, password);
    }

    public boolean usernameExists(String username) throws IOException, SQLException {
        return (userDao.getUserByUsername(username) != null);
    }

    public boolean emailExists(String email) throws IOException, SQLException {
        return (userDao.getUserByEmail(email) != null);
    }
}
