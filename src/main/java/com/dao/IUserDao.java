package com.dao;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import com.models.User;
import com.util.Connector;

public class IUserDao implements UserDao {
    @Override
    public User getUserByUserId(int userId) throws IOException, SQLException {
        User user = null;

        try (Connection conn = Connector.getInstance().getConnection();) {
            String sql = "SELECT * FROM ers_users WHERE ers_users_id = ?";

            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, userId);

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                user = createUserFromResultSet(rs);
            }
        }

        return user;
    }

    @Override
    public User getUserByUsername(String username) throws IOException, SQLException {
        User user = null;

        try (Connection conn = Connector.getInstance().getConnection();) {
            String sql = "SELECT * FROM ers_users WHERE ers_username = ?";

            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, username);

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                System.out.println("In while{getUserByUsername()}");
                user = createUserFromResultSet(rs);
            }
        }

        return user;
    }

    @Override
    public User getUserByEmail(String email) throws IOException, SQLException {
        User user = null;

        try (Connection conn = Connector.getInstance().getConnection();) {
            String sql = "SELECT * FROM ers_users WHERE user_email = ?";

            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, email);

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                user = createUserFromResultSet(rs);
            }
        }

        return user;
    }

    @Override
    public User getUserByCredentials(String username, String password) throws IOException, SQLException {
        User user = null;

        try (Connection conn = Connector.getInstance().getConnection();) {
            String sql = "SELECT * FROM ers_users WHERE ers_username = ? AND ers_password = ?";

            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1,  username);
            preparedStatement.setString(2,  password);

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                user = createUserFromResultSet(rs);
            }
        }

        return user;
    }

    @Override
    public User addUser(User user) throws IOException, SQLException {
        try (Connection conn = Connector.getInstance().getConnection();) {
            conn.setAutoCommit(false);
            String sql = ""
                    + "INSERT INTO ers_users"
                    + "(ers_username, ers_password, user_first_name, user_last_name, user_email, user_role_id)"
                    + "VALUES (?,?,?,?,?,?)";

            PreparedStatement preparedStatement = conn.prepareStatement(sql, new String[]{"ers_users_id"});

            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getFirstName());
            preparedStatement.setString(4, user.getLastName());
            preparedStatement.setString(5, user.getEmail());
            preparedStatement.setInt(6, user.getRoleId());

            int rowsInserted = preparedStatement.executeUpdate();
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();

            if (rowsInserted != 0) {
                while (generatedKeys.next()) {
                    user.setUserId(generatedKeys.getInt(1));
                }

                conn.commit();
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw e;
        }

        return user;
    }


    @Override
    public User updateUser(User user) throws IOException, SQLException {
        try (Connection conn = Connector.getInstance().getConnection();) {
            String sql = ""
                    + "UPDATE ers_users"
                    + "SET"
                    + "ers_username = ?,"
                    + "ers_password = ?,"
                    + "user_first_name = ?,"
                    + "user_last_name = ?,"
                    + "user_email = ?,"
                    + "user_role_id = ?"
                    + "WHERE ers_users_id = ?";

            PreparedStatement preparedStatement = conn.prepareStatement(sql);

            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getFirstName());
            preparedStatement.setString(4, user.getLastName());
            preparedStatement.setString(5, user.getEmail());
            preparedStatement.setInt(6, user.getRoleId());
            preparedStatement.setInt(7, user.getUserId());

            int rowsUpdated = preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.getResultSet();

            while (rs.next()) {
                user = createUserFromResultSet(rs);
            }
        } catch (SQLException e) {
            throw e;
        }

        return user;
    }

    @Override
    public List<User> getAllUsersList() throws IOException, SQLException {
        List<User> usersList = new ArrayList<>();

        try (Connection conn = Connector.getInstance().getConnection();) {
            String sql = "SELECT * FROM ers_users";

            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                usersList.add(createUserFromResultSet(rs));
            }
        } catch (SQLException e) {
            throw e;
        }

        return usersList;
    }

    private User createUserFromResultSet(ResultSet rs) throws SQLException {
        User user = new User();
        user.setUserId(rs.getInt("ers_users_id"));
        System.out.println("LOG" + rs.getInt("ers_users_id"));
        user.setUsername(rs.getString("ers_username"));
        user.setPassword(rs.getString("ers_password"));
        user.setFirstName(rs.getString("user_first_name"));
        user.setLastName(rs.getString("user_last_name"));
        user.setEmail(rs.getString("user_email"));
        user.setRoleId(rs.getInt("user_role_id"));
        return user;
    }


}
