package com.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.models.Reimbursement;
import com.util.Connector;

public class IReimbDao implements ReimbDao{
    Reimbursement reimb;
    List<Reimbursement> reimbList;

    @Override
    public Reimbursement getReimbById(int id) throws SQLException {
        reimb = null;

        try (Connection conn = Connector.getInstance().getConnection()) {
            String sql = "SELECT * FROM ers_reimbursement WHERE reimb_id = ?";

            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, id);

            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                reimb = createReimb(rs);
            }
        } catch (SQLException e) {
            throw e;
        }

        return reimb;
    }

    @Override
    public Reimbursement addReimb(Reimbursement reimb) throws SQLException {
        try (Connection conn = Connector.getInstance().getConnection()) {
            conn.setAutoCommit(false);

            String sql = "INSERT INTO ers_reimbursement (reimb_amount, reimb_submitted, reimb_resolved," +
                    "reimb_description, reimb_receipt, reimb_author," +
                    "reimb_status_id, reimb_type_id) VALUES" +
                    "(?, ?, ?, ?, ?, ?, ?, ?)";

            String[] keys = new String[1];
            keys[0] = "reimb_id";

            PreparedStatement preparedStatement = conn.prepareStatement(sql, keys);
            preparedStatement.setDouble(1, reimb.getAmount());
            preparedStatement.setTimestamp(2, reimb.getSubmitted());
            preparedStatement.setTimestamp(3, reimb.getResolved());
            preparedStatement.setString(4, reimb.getDescription());
            preparedStatement.setBlob(5, reimb.getReceipt());
            preparedStatement.setInt(6, reimb.getAuthor());
            preparedStatement.setInt(7, reimb.getStatusId());
            preparedStatement.setInt(8, reimb.getTypeId());

            int rowsInserted = preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.getGeneratedKeys();

            if (rowsInserted != 0) {
                while (rs.next()) {
                    reimb.setReimbId(rs.getInt(1));
                }

                conn.commit();
            }
        } catch (SQLException e) {
            throw e;
        }

        return reimb;
    }

    @Override
    public boolean updateReimb(Reimbursement reimb) throws SQLException {
        try (Connection conn = Connector.getInstance().getConnection()) {
            conn.setAutoCommit(false);

            String sql = "UPDATE ers_reimbursement SET reimb_amount = ?, reimb_submitted = ?, reimb_resolved = ?," +
                    "reimb_description = ?, reimb_receipt = ?, reimb_author = ?, reimb_resolver = ?," +
                    "reimb_status_id = ?, reimb_type_id = ? WHERE reimb_id = ?";

            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setDouble(1, reimb.getAmount());
            preparedStatement.setTimestamp(2, reimb.getSubmitted());
            preparedStatement.setTimestamp(3, reimb.getResolved());
            preparedStatement.setString(4, reimb.getDescription());
            preparedStatement.setBlob(5, reimb.getReceipt());
            preparedStatement.setInt(6, reimb.getAuthor());
            preparedStatement.setInt(7, reimb.getResolver());
            preparedStatement.setInt(8, reimb.getStatusId());
            preparedStatement.setInt(9, reimb.getTypeId());
            preparedStatement.setInt(10, reimb.getReimbId());

            int rowsInserted = preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.getGeneratedKeys();

            if (rowsInserted != 0) {
                conn.commit();

                return true;
            }
        } catch (SQLException e) {
            throw e;
        }

        return false;
    }

    @Override
    public List<Reimbursement> getReimbsByAuthor(int author) throws SQLException {
        reimbList = new ArrayList<>();

        try (Connection conn = Connector.getInstance().getConnection()) {
            String sql = "SELECT * FROM ers_reimbursement WHERE reimb_author = " + author + " ORDER BY reimb_submitted DESC";

            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                Reimbursement reimb = new Reimbursement();
                reimbList.add(createReimb(rs));
            }
        } catch (SQLException e) {
            throw e;
        }

        return reimbList;
    }

    @Override
    public List<Reimbursement> getReimbsByResolver(int resolver) throws SQLException {
        reimbList = new ArrayList<>();

        try (Connection conn = Connector.getInstance().getConnection()) {
            String sql = "SELECT * FROM ers_reimbursement WHERE reimb_resolver = " + resolver + " ORDER BY reimb_submitted DESC";

            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                Reimbursement reimb = new Reimbursement();
                reimbList.add(createReimb(rs));
            }
        } catch (SQLException e) {
            throw e;
        }

        return reimbList;
    }

    @Override
    public List<Reimbursement> getAllReimbs() throws SQLException {
        reimbList = new ArrayList<>();

        try (Connection conn = Connector.getInstance().getConnection()) {
            String sql = "SELECT * FROM ers_reimbursement ORDER BY reimb_submitted DESC";

            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                Reimbursement reimb = new Reimbursement();
                reimbList.add(createReimb(rs));
            }
        }

        return reimbList;
    }

    private Reimbursement createReimb(ResultSet rs) throws SQLException {
        Reimbursement reimb = new Reimbursement();

        reimb.setReimbId(rs.getInt("reimb_id"));
        reimb.setAmount(rs.getDouble("reimb_amount"));
        reimb.setSubmitted(rs.getTimestamp("reimb_submitted"));
        reimb.setResolved(rs.getTimestamp("reimb_resolved"));
        reimb.setDescription(rs.getString("reimb_description"));
        reimb.setReceipt(rs.getBlob("reimb_receipt"));
        reimb.setAuthor(rs.getInt("reimb_author"));
        reimb.setResolver(rs.getInt("reimb_resolver"));
        reimb.setStatusId(rs.getInt("reimb_status_id"));
        reimb.setTypeId(rs.getInt("reimb_type_id"));

        return reimb;
    }
}
