package com.dao;

import com.models.User;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface UserDao {

    User getUserByUserId(int userId) throws IOException, SQLException;
    User getUserByUsername(String username) throws IOException, SQLException;
    User getUserByCredentials(String username, String password) throws IOException, SQLException;

    User addUser(User user) throws IOException, SQLException;
    User updateUser(User user) throws IOException, SQLException;

    List<User> getAllUsersList() throws IOException, SQLException;
    User getUserByEmail(String email) throws IOException, SQLException;

}