package com.dao;

import com.models.Reimbursement;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


public interface ReimbDao {

    Reimbursement getReimbById(int id) throws IOException, SQLException;
    Reimbursement addReimb(Reimbursement reimb) throws IOException, SQLException;
    boolean updateReimb(Reimbursement reimb) throws IOException, SQLException;
    List<Reimbursement> getReimbsByAuthor(int author) throws IOException, SQLException;
    List<Reimbursement> getReimbsByResolver(int resolver) throws IOException, SQLException;
    List<Reimbursement> getAllReimbs() throws IOException, SQLException;

}
