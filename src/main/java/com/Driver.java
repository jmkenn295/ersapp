package com;

import com.models.User;
import com.services.UserService;

public class Driver {
    public static void main(String[] args) throws Exception {
        User newUser = new User("jmkenn295", "password123", "Mark", "Kennedy", "jmkenn295@gmail.com", 1);

        UserService userService = new UserService();
        newUser = userService.addUser(newUser);
        System.out.println(newUser);
    }
}
