package com.util;

import javax.servlet.http.HttpServletRequest;

public class Viewer {
    public static String process(HttpServletRequest request) {

        // Return corresponding partial
        switch (request.getRequestURI()) {
            case "/ERS-App/login.view":
                return "partials/login.html";
            case "/ERS-App/registration.view":
                return "partials/registration.html";
            case "/ERS-App/user_dashboard.view":
                return "partials/user_dashboard.html";
            case "/ERS-App/manager_dashboard.view":
                return "partials/manager_dashboard.html";
            case "/ERS-App/new_reimbursement.view":
                return "partials/new_reimbursement.html";
            case "/ERS-App/reimbursement_detail.view":
                return "partials/reimbursement_detail.html";
            default:
                return null;
        }
    }
}
