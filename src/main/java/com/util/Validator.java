package com.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.services.UserService;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.SQLException;

public class Validator {
    public static String process(HttpServletRequest request) throws IOException, SQLException {
        UserService userService = new UserService();
        ObjectMapper mapper = new ObjectMapper();

        switch(request.getRequestURI()) {

            case "/ERS-App/username.validate":
                String username = mapper.readValue(request.getInputStream(), String.class);

                if(userService.usernameExists(username)) {
                    return username;
                }
                else return null;

            case "/ERS-App/email.validate":
                String emailAddress = mapper.readValue(request.getInputStream(), String.class);

                if(userService.emailExists(emailAddress)) {
                    System.out.println(emailAddress);
                    return emailAddress;
                }
                else return null;

            default:
                return null;
        }
    }
}
