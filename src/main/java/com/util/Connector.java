package com.util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class Connector {
    private static Connector cf;
    private static boolean build = true;

    private Connector() {
        build = false;
    }

    public static synchronized Connector getInstance() {
        return (build) ? cf = new Connector() : cf;
    }

    public Connection getConnection() {

        Connection conn = null;
        Properties prop = new Properties();

        //todo change this for postgresql
        try {
            // Load the properties file keys/values into the Properties object
            Class.forName("oracle.jdbc.driver.OracleDriver");
            prop.load(new FileReader("/Users/pauklennedy/Code/revature/ers-app/src/main/resources/application.properties"));

            // Get a connection from the DriverManager
            conn = DriverManager.getConnection(
                    prop.getProperty("url"),
                    prop.getProperty("usr"),
                    prop.getProperty("pw"));

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return conn;
    }
}
