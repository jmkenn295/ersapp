package com.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.models.User;
import com.services.UserService;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

public class RegistrationServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserService userService = new UserService();
		ObjectMapper mapper = new ObjectMapper();
		
		User newUser = mapper.readValue(request.getInputStream(), User.class);
		
		try {
			newUser = userService.addUser(newUser);
			newUser.setPassword("****************");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (newUser != null) {
			HttpSession session = request.getSession();
			session.setAttribute("user", newUser);
		}

		response.setContentType("application/json");
		response.getWriter().write(mapper.writeValueAsString(newUser));
	}
}
