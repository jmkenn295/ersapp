package com.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.models.Reimbursement;
import com.services.ReimbursementService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

@WebServlet("/reimbursements")
public class ReimbursementsServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		ObjectMapper mapper = new ObjectMapper();
		ReimbursementService reimbService = new ReimbursementService();
		
		Reimbursement reimbursement = mapper.readValue(request.getInputStream(), Reimbursement.class);
		reimbursement.setSubmitted(new Timestamp(System.currentTimeMillis()));
		
		try {
			reimbursement = reimbService.addReimbursement(reimbursement);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		response.setContentType("application/json");
		response.getWriter().write(mapper.writeValueAsString(reimbursement));
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		ObjectMapper mapper = new ObjectMapper();
		ReimbursementService reimbService = new ReimbursementService();
		List<Reimbursement> reimbursementsList = null;
		
		try {
			reimbursementsList = reimbService.getAllReimbs();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		response.setContentType("application/json");
		response.getWriter().write(mapper.writeValueAsString(reimbursementsList));
	}

}
