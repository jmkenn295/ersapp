package com.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.models.Reimbursement;
import com.services.ReimbursementService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;

@WebServlet("/resolve_reimb")
public class ResolveReimbServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		ObjectMapper mapper = new ObjectMapper();
		Reimbursement reimb = mapper.readValue(request.getInputStream(), Reimbursement.class);
		boolean success = false;

		try {
			reimb.setResolved(new Timestamp(System.currentTimeMillis()));
			success = ReimbursementService.updateReimbursement(reimb);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		response.setContentType("application/json");
		response.getWriter().write(mapper.writeValueAsString(success));
	}
}
