package com.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.models.Reimbursement;
import com.services.ReimbursementService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

@WebServlet("/reimbursementsAuthor/*")
public class ReimbursementsAuthorServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		String uri = request.getRequestURI();
		String[] uriArray = uri.split("/");
		int author = Integer.parseInt(uriArray[uriArray.length - 1]);
		
		List<Reimbursement> reimbursementsList = null;
		
		try {
			reimbursementsList = ReimbursementService.getReimbsByAuthor(author);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		PrintWriter pw = response.getWriter();
		response.setContentType("application/json");
		String reimbursementsListJSON = mapper.writeValueAsString(reimbursementsList);
		pw.write(reimbursementsListJSON);
	}
}
