package com.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.models.User;
import com.services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		String[] credentials = mapper.readValue(request.getInputStream(), String[].class);
		
		UserService userService = new UserService();
		User user = null;
		
		try {
			user = userService.getUserByCredentials(credentials[0], credentials[1]);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (user != null) {
			user.setPassword("****************");
			HttpSession session = request.getSession();
			session.setAttribute("user", user);
		}
		
		response.setContentType("application/json");
		response.getWriter().write(mapper.writeValueAsString(user));
		
	}
}
