package com.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.services.UserService;
import com.models.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet("/users/*")
public class UserServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		String uri = request.getRequestURI();
		String[] uriArray = uri.split("/");
		int userId = Integer.parseInt(uriArray[uriArray.length - 1]);
		
		UserService userService = new UserService();
		User user = null;
		
		try {
			user = userService.getUserByUserId(userId);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		PrintWriter pw = response.getWriter();
		response.setContentType("application/json");
		String userJSON = mapper.writeValueAsString(user);
		pw.write(userJSON);
	}
}
