import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { AuthenticationRoutingModule } from './authentication-routing.module';
import { AuthService } from './services/auth.service';

@NgModule({
  imports: [
    CommonModule,
    AuthenticationRoutingModule,
    FormsModule
  ],
  providers: [AuthService],
  declarations: [LoginComponent]
})
export class AuthenticationModule { }
