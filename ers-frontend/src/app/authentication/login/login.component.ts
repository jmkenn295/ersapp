import { Component, OnInit, enableProdMode } from '@angular/core';
import { AuthService } from '../auth.service';
import { Location } from '@angular/common';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;

  constructor(private auth: AuthService,
    private location: Location){}
    
    ngOnInit() {
      if (this.auth.isLoggedIn()){
        this.location.go(environment.successLoginUrl)
      }
    }a
    
    loginSubmit(){
      
      event.preventDefault()
      this.auth.authenticate(this.username, this.password,
        () => this.location.go(environment.successLoginUrl),
        (err) => {
          console.log(err);
          this.location.go(`${environment.failLoginUrl}?status=${err.error}`)
        })
      
    }
    
  }
  