create table if not exists ers_reimbursement (
	remimb_id serial primary key,
	reimb_amount integer,
	reimb_submitted timestamp,
	reimb_resolved timestamp,
	reimb_description varchar(300),
	reimb_receipt bytea,
	reimb_author integer references ers_users(ers_users_id),
	reimb_resolver integer references ers_users(ers_users_id),
	reimb_status_id integer references ers_reimbursement_status(reimb_status_id),
	reimb_type_id integer references ers_reimbursement_type(reimb_type_id)
);

create table if not exists ers_users(
	ers_users_id serial primary key,
	ers_username varchar(50),
	ers_password varchar,
	user_first_name varchar(100),
	user_last_name varchar(100),
	user_email varchar(150),
	user_role_id integer references ers_user_roles(ers_user_role_id)
);

create table if not exists ers_reimbursement_status (
	reimb_status_id serial primary key,
	reimb_status varchar(10)
);

create table if not exists ers_reimbursement_type (
	reimb_type_id serial primary key,
	reimb_type varchar (10)
);

create table if not exists ers_users_roles (
	ers_user_role_id serial primary key,
	user_role varchar(10)
);


